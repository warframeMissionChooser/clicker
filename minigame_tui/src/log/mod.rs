use arraydeque::{ArrayDeque, Wrapping};
use crossbeam_channel::{unbounded, Receiver};
use tracing_subscriber::FmtSubscriber;
use tui::{
    buffer::Buffer,
    layout::{Alignment, Rect},
    text::Spans,
    widgets::{Widget, Wrap},
};

/// Subscriber that will receive, render and send the event
mod subscriber;

/// Handle log reception, history keeping and rendering of logs
pub struct Display {
    /// Channel from which new logs are gathered
    rcv: Receiver<Spans<'static>>,

    /// History of log entries gathered
    /// Ring buffer that will keep at most 100 entries
    logs: ArrayDeque<[Spans<'static>; 100], Wrapping>,
}

impl Display {
    /// Create a new log display
    /// Will return None if a log subscriber has already been set, thus this function should only be called once
    pub fn new() -> Option<Self> {
        use tracing_subscriber::layer::SubscriberExt;

        let (snd, rcv) = unbounded();

        let logger = FmtSubscriber::builder()
            .with_writer(std::io::sink)
            .finish()
            .with(subscriber::Log::new(snd));

        tracing::dispatcher::set_global_default(logger.into()).ok()?;

        Some(Self {
            rcv,
            logs: ArrayDeque::new(),
        })
    }

    /// Update the log gatherer
    /// Should be called once per frame, before rendering if possible
    pub fn update(&mut self) {
        while let Ok(log) = self.rcv.try_recv() {
            self.logs.push_front(log);
        }
    }
}

impl Widget for &Display {
    #[inline]
    fn render(self, area: Rect, buf: &mut Buffer) {
        use tui::widgets::{Block, BorderType, Borders, Paragraph};

        let block = Block::default()
            .title(" Logs  ")
            .border_type(BorderType::Rounded)
            .borders(Borders::ALL);

        Paragraph::new(self.logs.iter().cloned().collect::<Vec<Spans>>())
            .block(block)
            .alignment(Alignment::Left)
            .wrap(Wrap { trim: true })
            .render(area, buf);
    }
}
