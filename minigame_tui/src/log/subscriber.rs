use tracing::Subscriber;
use tracing_subscriber::Layer;
use tui::{
    style::{Color, Modifier, Style},
    text::{Span, Spans},
};

use crossbeam_channel::Sender;

use tracing::Event;
use tracing_subscriber::{layer::Context, registry::LookupSpan};

/// A Log Subscriber that will send event on a channel
pub struct Log {
    /// The channel on which the event will be sent
    snd: Sender<Spans<'static>>,
}

impl Log {
    /// Create a new Log Subscriber that will send event on the channel provided
    pub fn new(snd: Sender<Spans<'static>>) -> Self {
        Self { snd }
    }
}

impl<S> Layer<S> for Log
where
    S: Subscriber + for<'lookup> LookupSpan<'lookup>,
{
    fn on_event(&self, event: &Event<'_>, ctx: Context<'_, S>) {
        let msg = vec![
            // Date
            Span::styled(
                chrono::Local::now().format("%H:%M:%S%.3f ").to_string(),
                Style::default().fg(Color::DarkGray),
            ),
            // Log Level
            {
                let level = event.metadata().level();
                Span::styled(
                    format!("{:^5} ", level),
                    Style::default().fg(match *level {
                        tracing::Level::TRACE => Color::Magenta,
                        tracing::Level::DEBUG => Color::Blue,
                        tracing::Level::INFO => Color::Green,
                        tracing::Level::WARN => Color::Yellow,
                        tracing::Level::ERROR => Color::Red,
                    }),
                )
            },
            // Context
            Span::styled(
                {
                    let span = event
                        .parent()
                        .and_then(|id| ctx.span(id))
                        .or_else(|| ctx.lookup_current());

                    let mut context = span
                        .into_iter()
                        .flat_map(|span| {
                            span.from_root().chain(std::iter::once(span))
                        })
                        .map(|span| span.metadata().name().to_owned())
                        .fold(String::new(), |mut full, name| {
                            if !full.is_empty() {
                                full.push(':');
                            }
                            full.push_str(&name);

                            full
                        });

                    context.push(' ');
                    context
                },
                Style::default().add_modifier(Modifier::BOLD),
            ),
            // Target
            Span::styled(
                format!("{} ", event.metadata().target()),
                Style::default().fg(Color::DarkGray),
            ),
            // Fields
            Span::from({
                /// A strucutre that will visit each field, and store their debug representation in a buffer
                struct Visitor {
                    /// A buffer containing all Debug representation of all field
                    buffer: String,
                }
                impl tracing_subscriber::field::Visit for Visitor {
                    fn record_debug(
                        &mut self,
                        field: &tracing::field::Field,
                        value: &dyn std::fmt::Debug,
                    ) {
                        if !self.buffer.is_empty() {
                            self.buffer += ", ";
                        }
                        self.buffer += &match field.name() {
                            "message" => format!("{:?}", value),
                            field => format!("{} = {:?}", field, value),
                        }
                    }
                }

                let mut visitor = Visitor {
                    buffer: String::new(),
                };
                event.record(&mut visitor);

                visitor.buffer
            }),
        ];

        match self.snd.send(msg.into()) {
            Ok(()) => (),
            Err(err) => println!("Can't send log: {:?}", err),
        }
    }
}
