use tui::style::Color;
use tui::widgets::canvas::{Line, Shape};

/// Shape to draw a centered circle with a given resolution, radius and color
pub struct Circle {
    /// Resolution of the circle. This is the number of line that will be drawn to draw the circle
    /// 2 will make a line, 4 a square. A value of 50 usually look good
    /// Higher mean better quality, but is slower
    pub resolution: u16,

    /// Radius of the circle
    pub radius: f64,

    /// Color of the circle
    pub color: Color,
}

impl Default for Circle {
    #[inline]
    fn default() -> Self {
        Self {
            resolution: 50,
            radius: 1.0,
            color: Color::Reset,
        }
    }
}

impl Circle {
    /// Compute the angle increment between two points
    #[inline]
    fn increment(&self) -> f64 {
        use std::f64::consts::PI;

        let resolution: f64 = self.resolution.into();
        2.0 * PI / resolution
    }

    /// Return an iterator over all angles for the circle
    #[inline]
    pub fn angles(&self) -> impl Iterator<Item = f64> {
        let increment = self.increment();

        (0..self.resolution).map(move |i| {
            let f: f64 = i.into();
            f * increment
        })
    }
}

impl Shape for Circle {
    #[inline]
    fn draw(&self, painter: &mut tui::widgets::canvas::Painter) {
        let increment = self.increment();

        for from in self.angles() {
            let to: f64 = from + increment;
            Line {
                x1: from.cos() * self.radius,
                y1: from.sin() * self.radius,
                x2: to.cos() * self.radius,
                y2: to.sin() * self.radius,
                color: self.color,
            }
            .draw(painter)
        }
    }
}
