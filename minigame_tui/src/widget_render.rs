use tui::{
    buffer::Buffer,
    layout::Rect,
    text::Spans,
    widgets::{canvas, Block, BorderType, Borders, Widget},
};

/// A structure that hold a renderer
pub struct WidgetRender(Box<dyn FnOnce(Rect, &mut Buffer)>);

impl WidgetRender {
    /// Create a new `WidgetRender` from a `Widget`
    #[inline]
    pub fn new<T: Widget + 'static>(val: T) -> Self {
        Self::new_fn(|area, buf| val.render(area, buf))
    }

    /// Create a new `WidgetRender` from a function that draw on the screen from a `Rect` and a `Buffer`
    #[inline]
    pub fn new_fn<T: FnOnce(Rect, &mut Buffer) + 'static>(func: T) -> Self {
        Self(Box::new(func))
    }

    /// Create a new `WidgetRender` that render a canvas. This take a function that take a `Context` and will draw on it.
    /// The canvas size is [-1; 1] on both axis
    #[inline]
    pub fn new_centered_canvas<T, F>(title: T, func: F) -> Self
    where
        T: Into<Spans<'static>>,
        F: Fn(&mut canvas::Context) + 'static,
    {
        use tui::layout::Margin;
        use tui::widgets::canvas::Canvas;

        let block = Block::default()
            .title(title)
            .border_type(BorderType::Rounded)
            .borders(Borders::ALL);

        Self::new_fn(move |area, buf| {
            let min = area.height.min(area.width.wrapping_div(2));
            let area = area.inner(&Margin {
                horizontal: area.width.wrapping_div(2).saturating_sub(min),
                vertical: area.height.saturating_sub(min).wrapping_div(2),
            });
            Canvas::default()
                .block(block)
                .x_bounds([-1.0, 1.0])
                .y_bounds([-1.0, 1.0])
                .paint(func)
                .render(area, buf)
        })
    }
}

impl Widget for WidgetRender {
    #[inline]
    fn render(self, area: Rect, buf: &mut Buffer) {
        self.0(area, buf)
    }
}
