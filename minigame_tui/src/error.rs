use thiserror::Error;

/// Error type for TUI operation
#[derive(Debug, Error)]
pub enum Error {
    /// IO Error while writing to Terminal
    #[error("IO Error while writing to Terminal")]
    Disconnect(#[from] std::io::Error),

    /// Unable to set the global log subscriber
    #[error("Unable to set the global log subscriber")]
    UnableToCreateLogGather,
}

/// A specialized Result type for TUI operations.
pub type Result<T> = std::result::Result<T, Error>;
