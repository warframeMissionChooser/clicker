use image::{GenericImageView, Pixel};

/// Compute the average color of an image
/// Return the average for each subpixel as a Vec
#[inline]
pub fn average_color<I>(img: &I) -> Vec<f64>
where
    I: GenericImageView,
    <<I as image::GenericImageView>::Pixel as image::Pixel>::Subpixel:
        From<u8> + Into<f64>,
{
    let total = img.pixels().map(|(_, _, color)| color).fold(
        vec![0.0; 4],
        |total, v| {
            v.channels()
                .iter()
                .zip(total)
                .map(|(&subpixel, total)| {
                    let subpixel: f64 = subpixel.into();
                    subpixel + total
                })
                .collect()
        },
    );

    let (w, h) = img.dimensions();
    let size: f64 = w.saturating_mul(h).into();

    total.iter().map(|e| e / size).collect()
}
