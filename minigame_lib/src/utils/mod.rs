/// Function that search compute the likeliness of a view of an image relative to a reference pattern
mod kernel_finder;
pub use kernel_finder::KernelFinder;

/// Function that compute the avaerage color of an image
mod average_color;
pub use average_color::average_color;
