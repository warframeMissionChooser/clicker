use image::{GenericImageView, RgbImage};
use imageproc::stats::root_mean_squared_error;

/// Search location of the kernel
struct SearchLocation {
    /// X location in the image
    x: u32,
    /// Y location in the image
    y: u32,
    /// Width of the pattern
    width: u32,
    /// Heigth of the pattern
    height: u32,
}

/// `KernelFinder` structure that allow to search if a specific image is present at a certain location in a big image
pub struct KernelFinder {
    /// Image to search
    kernel: RgbImage,
    /// Location within the whole image to search for the kernel
    location: SearchLocation,
    /// Likeliness value required to be considered as found
    threshold: f64,
}

impl KernelFinder {
    /// Create a new `KernelFinder` from a kernel image, a location and a threshold
    #[inline]
    #[must_use]
    pub fn new(kernel: RgbImage, location: [u32; 2], threshold: f64) -> Self {
        Self {
            location: SearchLocation {
                x: location[0],
                y: location[1],
                width: kernel.width(),
                height: kernel.height(),
            },
            kernel,
            threshold,
        }
    }

    /// Check if the kernel is found in the provided image
    #[inline]
    #[must_use]
    pub fn check(&self, img: &RgbImage) -> bool {
        let part = img
            .view(
                self.location.x,
                self.location.y,
                self.location.width,
                self.location.height,
            )
            .to_image();

        let diff = root_mean_squared_error(&part, &self.kernel);

        diff < self.threshold
    }
}
