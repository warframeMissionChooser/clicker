//! Library used by Warframe Clicker. Contains the runner and the `MiniGame` trait
//! Implementation of Games are provided as separate crate
//! A binary is provided in the crate: `minigame`

#![deny(clippy::all)]
#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]
#![allow(clippy::multiple_crate_versions)]

pub use enigo;
pub use image;
pub use imageproc;
pub use minigame_tui as tui;
pub use tracing;

/// `Minigame` trait
pub mod minigames;

/// Utility functions used by minigames implementations
pub mod utils;
