pub use minigame_tui::WidgetRender;

pub use enigo::Enigo;
pub use image::DynamicImage;

use std::any::Any;

/// Implementation of a minigame
/// # Lifecicle of a minigame
/// The `is_open` method will be called until it return true
/// Once this method returned true, the `start` method will be called
/// Then the `handle` and `render` method will be called until `handle` returns false
/// Thern the `end` method will be called
///
/// A basic runner would look like:
/// ```compile_fail
/// let game: MiniGame = todo!();
/// loop {
///     while !game.should_open() {}
///     game.start();
///     while game.handle() {
///         game.render();
///     }
///     game.close();
/// }
/// ```
pub trait MiniGame {
    type Running: RunningMiniGame<Self>;

    /// Name of the minigame
    const NAME: &'static str;

    /// Test if the game is considered to be open
    fn should_open(&self, img: &DynamicImage) -> Option<Self::Running>;
}

pub trait RunningMiniGame<Root: ?Sized>: Any {
    fn start(&mut self, _enigo: &mut Enigo) {}
    fn end(&mut self, _enigo: &mut Enigo) {}

    /// Handle the state
    /// Use the enigo param to dispatch mouse and keyboard event
    fn handle(
        &mut self,
        root: &Root,
        img: &DynamicImage,
        enigo: &mut Enigo,
    ) -> bool;

    /// Provide a `WidgetRender` that can be used as a debug diaply of the state of the opened minigame
    fn render(&self) -> WidgetRender;
}
