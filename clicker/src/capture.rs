use minigame_lib::image::{DynamicImage, ImageBuffer};
use scrap::{Capturer, Display};
use tracing::{error, instrument};

use std::convert::TryInto;
use std::io;

pub struct Capture {
    cap: Capturer,
}

impl Capture {
    pub fn new() -> io::Result<Self> {
        Ok(Self {
            cap: Capturer::new(Display::primary()?)?,
        })
    }
}

impl Iterator for Capture {
    type Item = DynamicImage;

    #[instrument(name = "Capture::next_frame", level = "trace", skip(self))]
    fn next(&mut self) -> Option<Self::Item> {
        use std::io::ErrorKind::WouldBlock;

        let frame: Vec<u8> = loop {
            match self.cap.frame() {
                Ok(frame) => break frame.to_vec(),
                Err(e) if e.kind() == WouldBlock => continue,
                Err(err) => {
                    error!(%err);
                    continue;
                }
            };
        };

        let img = DynamicImage::ImageBgra8(
            ImageBuffer::from_raw(
                self.cap
                    .width()
                    .try_into()
                    .expect("Size should fit in an u32"),
                self.cap
                    .height()
                    .try_into()
                    .expect("Size should fit in an u32"),
                frame,
            )
            .expect("Image returned by `scrap` should be valid"),
        );

        Some(img)
    }
}
