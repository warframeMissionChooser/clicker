#![deny(clippy::all)]
#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]
#![allow(clippy::multiple_crate_versions)]

mod capture;
use capture::Capture;

use clicker_runner::Runner;
use warframe_hacking_corpus::CorpusHacking;
use warframe_hacking_grineer::GrineerHacking;

use tc2_drag_race::DragRace;

fn main() {
    let mut runner = Runner::new().unwrap();

    runner.try_add_game(GrineerHacking::new());
    runner.try_add_game(CorpusHacking::new());
    runner.try_add_game(DragRace::new());

    let cap = Capture::new().expect("Unable to start capture");
    runner.run(cap);
}
