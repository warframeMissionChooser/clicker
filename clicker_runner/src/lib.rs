//! Runner for `MiniGame`s, user need to provide frames, but handle everything else

#![deny(clippy::all)]
#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]
#![allow(clippy::multiple_crate_versions)]

use minigame_lib::{
    enigo::Enigo,
    image::DynamicImage,
    minigames::MiniGame,
    tracing::{error, info},
    tui::{error::Result as R, WidgetRender, TUI},
};

/// Store information about current active game
mod current_game;
use current_game::CurrentGame;

mod minigame_notype;
use minigame_notype::MiniGameNoType;

/// `Runner` struct that will continuously capture frame, select which minigame should is currently played, and press the right key to solve this minigame
/// Contain a list of availible minigames
/// Will also render a terminal interface with debug info
pub struct Runner {
    /// List of games that this instance is supporting
    supported_games: Vec<Box<dyn MiniGameNoType>>,

    /// The Terminal User Interface used to render on screen debug info
    tui: TUI,

    /// The structure used to send Keyboard and mouse events
    enigo: Enigo,
}

impl Runner {
    /// Create a new empty runner
    /// <div class="information">
    ///     <div class="tooltip compile_fail" style="">&#x26a0; &#xfe0f;<span class="tooltiptext">Warning</span></div>
    /// </div>
    /// <div class="example-wrap" style="display:inline-block"><pre class="compile_fail" style="white-space:normal;font:inherit;">
    ///     *Should only be created once*
    /// </pre></div>
    ///
    /// # Errors
    /// An error will be returned if the Terminal Interface cannot be created.
    /// This can be because a Runner has already been created, or if stdout is not availible
    #[inline]
    pub fn new() -> R<Self> {
        Ok(Self {
            supported_games: Vec::new(),
            tui: TUI::new()?,
            enigo: Enigo::new(),
        })
    }

    /// Add a game to the list of supported games
    #[inline]
    pub fn add_game<G>(&mut self, g: G)
    where
        G: MiniGame + 'static,
    {
        self.supported_games.push(Box::new(g));
    }

    /// Try to add a game to the list of supported games
    /// If the provided game is an Error, it will display the error in the TUI
    #[inline]
    pub fn try_add_game<G, E>(&mut self, g: Result<G, E>)
    where
        G: MiniGame + 'static,
        E: std::error::Error,
    {
        match g {
            Ok(game) => self.add_game(game),
            Err(err) => error!("Unable to load game {}: {:?}", G::NAME, err),
        }
    }

    /// Start the capture loop
    /// This will capture image, detect the minigame and solve it
    /// It will also render the TUI
    #[inline]
    pub fn run<Cap>(&mut self, cap: Cap)
    where
        Cap: Iterator<Item = DynamicImage>,
    {
        info!("Starting Warframe Clicker");
        info!(available_games = ?self.supported_games.iter().map(|g| g.name()).collect::<Vec<_>>(), "Enabled minigames");

        cap.fold(None, |last_game, img| self.handle_frame(last_game, &img));
    }

    /// Return a minigame that is active base on the image provided
    #[inline]
    fn get_minigame(&mut self, img: &DynamicImage) -> Option<CurrentGame> {
        self.supported_games
            .iter_mut()
            .enumerate()
            .find_map(|(id, g)| {
                let is_open = g.should_open(img);

                is_open.map(|running_info| {
                    CurrentGame::new(id, g.as_ref(), running_info)
                })
            })
    }

    /// Compute the TUI Widget that should be displayed when no minigames are active
    #[inline]
    fn render(&self) -> WidgetRender {
        use minigame_lib::tui::{
            layout::Alignment,
            text::{Span, Spans, Text},
            widgets::{Block, BorderType, Borders, Paragraph, Widget},
        };
        use std::iter::{once, repeat};

        let block = Block::default()
            .title(" Runner  ")
            .border_type(BorderType::Rounded)
            .borders(Borders::ALL);

        let text: Vec<Spans> =
            once(Span::raw("Available games :"))
                .chain(self.supported_games.iter().map(|game| {
                    Span::raw(format!("\u{2022} {}", game.name(),))
                }))
                .map(Spans::from)
                .collect();

        WidgetRender::new_fn(|area, buf| {
            let inner = block.inner(area);
            let height: usize = inner.height.into();

            let num_line = text.len();
            let line_left = height.saturating_sub(num_line);

            let text: Vec<_> = repeat(Spans::default())
                .take(line_left.wrapping_div(2))
                .chain(text)
                .collect();

            Paragraph::new({
                let a: Text<'static> = text.into();
                a
            })
            .block(block)
            .alignment(Alignment::Center)
            .render(area, buf)
        })
    }

    /// Handle a frame when `last_game_id` contain information about last frame
    /// The returned value sould be given to the next call of `handle_frame`
    fn handle_frame(
        &mut self,
        last_game_id: Option<CurrentGame>,
        img: &DynamicImage,
    ) -> Option<CurrentGame> {
        if let Some(mut curr_game) =
            last_game_id.or_else(|| self.get_minigame(img))
        {
            let mut game = curr_game.upgrade(&mut self.supported_games);

            let res = game.handle(img, &mut self.enigo);

            match self.tui.render(game.render()) {
                Ok(()) => (),
                Err(err) => {
                    error!(?err, "Error while rendering {}", game.name())
                }
            }
            drop(game);

            if res {
                Some(curr_game.inc())
            } else {
                None
            }
        } else {
            match self.tui.render(self.render()) {
                Ok(()) => (),
                Err(err) => error!(?err, "Error while rendering Runner screen"),
            }

            None
        }
    }
}
