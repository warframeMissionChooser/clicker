use std::any::Any;

use minigame_lib::{
    enigo::Enigo,
    image::DynamicImage,
    minigames::{MiniGame, RunningMiniGame},
    tui::WidgetRender,
};

pub type BoxedAny = Box<dyn Any>;

fn get<T: MiniGame>(value: &BoxedAny) -> &T::Running {
    value.downcast_ref().unwrap_or_else(|| panic!(
        "Unable to cast in {}. Internal error",
        std::any::type_name::<T::Running>()
    ))
}

fn get_mut<T: MiniGame>(value: &mut BoxedAny) -> &mut T::Running {
    value.downcast_mut().unwrap_or_else(|| panic!(
        "Unable to cast in {}. Internal error",
        std::any::type_name::<T::Running>()
    ))
}

pub trait MiniGameNoType {
    fn name(&self) -> &'static str;
    fn should_open(&self, img: &DynamicImage) -> Option<BoxedAny>;

    fn start(&self, data: &mut BoxedAny, _enigo: &mut Enigo);
    fn end(&self, data: &mut BoxedAny, _enigo: &mut Enigo);
    fn handle(
        &self,
        data: &mut BoxedAny,
        img: &DynamicImage,
        enigo: &mut Enigo,
    ) -> bool;
    fn render(&self, data: &BoxedAny) -> WidgetRender;
}

impl<T: MiniGame> MiniGameNoType for T {
    fn name(&self) -> &'static str {
        Self::NAME
    }

    fn should_open(&self, img: &DynamicImage) -> Option<BoxedAny> {
        self.should_open(img).map(|e| Box::new(e) as Box<dyn Any>)
    }

    fn start(&self, data: &mut BoxedAny, enigo: &mut Enigo) {
        get_mut::<Self>(data).start(enigo);
    }

    fn end(&self, data: &mut BoxedAny, enigo: &mut Enigo) {
        get_mut::<Self>(data).end(enigo);
    }

    fn handle(
        &self,
        data: &mut BoxedAny,
        img: &DynamicImage,
        enigo: &mut Enigo,
    ) -> bool {
        get_mut::<Self>(data).handle(self, img, enigo)
    }

    fn render(&self, data: &BoxedAny) -> WidgetRender {
        get::<Self>(data).render()
    }
}
