use minigame_lib::{
    enigo::Enigo,
    image::DynamicImage,
    tracing::{info, span, Level, Span},
    tui::WidgetRender,
};

use crate::minigame_notype::{BoxedAny, MiniGameNoType};

/// Store information about the current minigame
/// This store only the index of the structure
/// This structure can persist across loop iteration, and can be upgraded to a `CurrentRealGame`
pub struct CurrentGame {
    /// Index of the game in the list
    id: usize,

    /// Span created at the start of this game
    span: Span,

    /// Number a frame since this game is active
    turn: usize,

    /// Information of the currently running game
    running_info: BoxedAny,
}

impl CurrentGame {
    /// Create a new `CurrentGame` based on index and the Minigame object
    pub fn new(
        id: usize,
        g: &dyn MiniGameNoType,
        running_info: BoxedAny,
    ) -> Self {
        Self {
            id,
            span: span!(Level::INFO, "Minigame", name = %g.name()),
            turn: 0,
            running_info,
        }
    }

    /// Return a `CurrentGame` with the turn value incremented
    #[allow(clippy::missing_const_for_fn)] // Can't be a const fn because of saturating_add
    pub fn inc(self) -> Self {
        Self {
            turn: self.turn.saturating_add(1),
            ..self
        }
    }

    /// Convert to a `CurrentRealGame` by adding a reference to the Minigame struct
    pub fn upgrade<'a>(
        &'a mut self,
        lst: &'a mut Vec<Box<dyn MiniGameNoType>>,
    ) -> CurrentRealGame<'a> {
        #[allow(clippy::expect_used)] // This is safe, otherwise, we have a bug
        CurrentRealGame {
            game: lst.get(self.id).expect("Id should be valid").as_ref(),
            _span: self.span.enter(),
            turn: self.turn,
            running_info: &mut self.running_info,
        }
    }
}

/// Extend `CurrentGame` with a reference to the Minigame object and an active span
/// This structure could only exist durting the body of the loop.
/// Only the `CurrentGame` structure will persist across loop
pub struct CurrentRealGame<'a> {
    /// The minigame actually running
    game: &'a (dyn MiniGameNoType + 'static),

    /// Number a frame since this game is active
    turn: usize,

    /// Information of the currently running game
    running_info: &'a mut BoxedAny,

    /// Active span
    _span: span::Entered<'a>,
}

impl<'a> CurrentRealGame<'a> {
    pub fn name(&self) -> &'static str {
        self.game.name()
    }

    pub fn handle(&mut self, img: &DynamicImage, enigo: &mut Enigo) -> bool {
        if self.turn == 0 {
            self.start(enigo)
        }

        let res = span!(Level::TRACE, "Handle image")
            .in_scope(|| self.game.handle(&mut self.running_info, img, enigo));

        if !res {
            self.end(enigo);
        };

        res
    }

    pub fn start(&mut self, enigo: &mut Enigo) {
        span!(Level::INFO, "Initialisation")
            .in_scope(|| self.game.start(&mut self.running_info, enigo));
    }

    pub fn end(&mut self, enigo: &mut Enigo) {
        info!("Terminating after {} frame", self.turn);

        span!(Level::INFO, "Terminating")
            .in_scope(|| self.game.end(&mut self.running_info, enigo));
    }

    pub fn render(&self) -> WidgetRender {
        let data = &self.running_info;
        self.game.render(data)
    }
}
