# Warframe Clicker

This is an helper for Warframe's minimap.

Just run the application, and it will detect when a supported minigame is found, it will then automatically solve it
```
git clone https://gitlab.com/warframeMissionChooser/clicker.git
cd clicker
cargo run --release
```


## Supported minigame

| Mini Games                       | Support |
|----------------------------------|---------|
| Grinner Hacking                  |  Basic  |
| Corpus Hacking                   | Todo #1 |
| Omni reparation                  | Todo #2 |