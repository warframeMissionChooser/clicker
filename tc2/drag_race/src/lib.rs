//! Crate to play a Drag Race run

#![deny(clippy::all)]
#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]
#![allow(clippy::multiple_crate_versions)]

use minigame_lib::{
    enigo::{Key, KeyboardControllable},
    image::{DynamicImage, GenericImageView, ImageResult},
    minigames::{Enigo, MiniGame, RunningMiniGame},
    tracing::{info, warn},
    tui::{
        layout::{Alignment, Constraint, Direction, Layout},
        style::{Color, Style},
        text::Span,
        widgets::{Block, BorderType, Borders, Paragraph, Widget},
        Buffer, Rect, WidgetRender,
    },
    utils::{average_color, KernelFinder},
};

#[derive(Debug, Clone)]
enum State {
    Burn,
    CountDown(usize),
    Run,
}

pub struct DragRace {
    kernel_find_tire: KernelFinder,
}

pub struct RunningDragRace {
    state: State,
    need_gear_up: bool,
    burn_status: bool,
    burn_delay: u8,
}

const TIRE_BURN_THRESHOLD: f64 = 50.0;
const TIRE_PATERN_LOCATION: [u32; 2] = [920, 986];

impl DragRace {
    /// Create a new Drag Race runner
    ///
    /// # Errors
    /// Will return an error if the saved image could not be read
    pub fn new() -> ImageResult<Self> {
        let kernel = {
            use minigame_lib::image::png::PngDecoder;

            static KERNEL_FILE: &[u8] = include_bytes!("kernel.png");
            let decoder = PngDecoder::new(KERNEL_FILE)?;
            DynamicImage::from_decoder(decoder)?.to_rgb8()
        };

        Ok(Self {
            kernel_find_tire: KernelFinder::new(
                kernel,
                TIRE_PATERN_LOCATION,
                TIRE_BURN_THRESHOLD,
            ),
        })
    }
}

impl MiniGame for DragRace {
    type Running = RunningDragRace;

    const NAME: &'static str = "Drag Race";

    fn should_open(&self, img: &DynamicImage) -> Option<Self::Running> {
        if self.kernel_find_tire.check(&img.to_rgb8()) {
            Some(RunningDragRace {
                state: State::Burn,
                need_gear_up: false,
                burn_status: false,
                burn_delay: 0,
            })
        } else {
            None
        }
    }
}

impl RunningMiniGame<DragRace> for RunningDragRace {
    fn end(&mut self, enigo: &mut Enigo) {
        enigo.key_up(Key::Layout('z'))
    }

    fn handle(
        &mut self,
        _root: &DragRace,
        img: &DynamicImage,
        enigo: &mut Enigo,
    ) -> bool {
        match self.state {
            State::Burn => {
                if let Some(state) = detect_countdown(img) {
                    self.state = state;
                } else {
                    self.burn_status = burn_status(img);
                }

                if self.burn_status {
                    if self.burn_delay == 0 {
                        enigo.key_up(Key::Layout('z'));
                    }

                    self.burn_delay = self.burn_delay.saturating_sub(1);
                } else {
                    enigo.key_down(Key::Layout('z'));
                    self.burn_delay = 5;
                }
            }
            State::CountDown(i) => {
                enigo.key_up(Key::Layout('z'));
                self.state = detect_countdown(img).unwrap_or(if i <= 1 {
                    State::Run
                } else {
                    State::CountDown(i)
                });
            }
            State::Run => {
                enigo.key_down(Key::Layout('z'));

                self.need_gear_up = gear_status(img);
                if self.need_gear_up {
                    info!("Gear UP !");
                    enigo.key_click(Key::Layout('e'));
                }
            }
        }

        // Detect the "R" of "Run" as end of game
        !is_white(&average_color(&img.view(1005, 315, 20, 78)))
    }

    fn render(&self) -> WidgetRender {
        let state = self.state.clone();
        let burn_ok = self.burn_status;
        let burn_delay = self.burn_delay;
        let need_gear_up = self.need_gear_up;

        WidgetRender::new_fn(move |area, buf| {
            let root = block(DragRace::NAME);
            let in_area = root.inner(area);
            root.render(area, buf);

            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(2)
                .constraints(
                    [Constraint::Length(3), Constraint::Percentage(3)].as_ref(),
                )
                .split(in_area);

            render_state(&state, chunks[0], buf);

            match state {
                State::Burn => render_burn(burn_ok, burn_delay, chunks[1], buf),
                State::Run => render_gear(need_gear_up, chunks[1], buf),
                State::CountDown(_) => (),
            }
        })
    }
}

const FIRST_POINT_DELTA: u32 = 90 + 12;

fn gear_status(img: &DynamicImage) -> bool {
    let img = img.view(878, 701, 12, 63);

    let color = average_color(&img);

    is_green(&color)
}

// Return if burn is OK
fn burn_status(img: &DynamicImage) -> bool {
    let bar_color = average_color(&img.view(849, 701, 4, 63));

    if is_green(&bar_color) {
        return true;
    }

    let too_far = average_color(&img.view(885, 557, 4, 4));

    if is_white(&too_far) {
        warn!("Burn went to far");
        return true;
    }

    false
}

fn detect_countdown(img: &DynamicImage) -> Option<State> {
    let width = img.width();
    for (id, color) in (0..4)
        .map(|i| (width / 2) - FIRST_POINT_DELTA + i * 60)
        .map(|x| img.view(x, 373, 24, 24))
        .map(|img| average_color(&img))
        .enumerate()
    {
        if is_red(&color) {
            continue;
        }

        if is_black(&color) {
            return State::CountDown(4 - id).into();
        }

        if is_green(&color) {
            return State::Run.into();
        }

        return None;
    }

    State::CountDown(0).into()
}

fn is_red(color: &[f64]) -> bool {
    color[0] > 240.0 && color[1] < 20.0 && color[2] < 70.0
}
fn is_black(color: &[f64]) -> bool {
    color[0] < 10.0 && color[1] < 10.0 && color[2] < 10.0
}
fn is_green(color: &[f64]) -> bool {
    color[0] < 40.0 && color[1] > 200.0 && color[2] < 140.0
}
fn is_white(color: &[f64]) -> bool {
    color[0] > 250.0 && color[1] > 250.0 && color[2] > 250.0
}

fn block(title: &str) -> Block {
    Block::default()
        .border_type(BorderType::Rounded)
        .borders(Borders::ALL)
        .title(format!(" {}  ", title))
}

fn render_state(state: &State, area: Rect, buf: &mut Buffer) {
    let step_status = block("Step status");

    let in_area = step_status.inner(area);

    step_status.render(area, buf);

    let span = match state {
        State::Burn => Span::styled("Burning", Style::default().fg(Color::Red)),
        State::CountDown(i) => Span::styled(
            format!("Countdown {}", i),
            Style::default().fg(Color::Yellow),
        ),
        State::Run => {
            Span::styled("Running", Style::default().fg(Color::Green))
        }
    };

    Paragraph::new(span)
        .alignment(Alignment::Center)
        .render(in_area, buf);
}

fn render_burn(burn_ok: bool, burn_delay: u8, area: Rect, buf: &mut Buffer) {
    let step_status = block("Burn Info");

    let in_area = step_status.inner(area);

    step_status.render(area, buf);

    let span = if burn_ok {
        if burn_delay == 0 {
            Span::styled("Tires Ready !", Style::default().fg(Color::Green))
        } else {
            Span::styled(
                format!("Tires still heating {}", burn_delay),
                Style::default().fg(Color::Yellow),
            )
        }
    } else {
        Span::styled("Heating tires", Style::default().fg(Color::Red))
    };

    Paragraph::new(span)
        .alignment(Alignment::Center)
        .render(in_area, buf);
}

fn render_gear(need_gear_up: bool, area: Rect, buf: &mut Buffer) {
    let step_status = block("Gear status");

    let in_area = step_status.inner(area);

    step_status.render(area, buf);

    let span = if need_gear_up {
        Span::styled("Gearing up", Style::default().fg(Color::Red))
    } else {
        Span::styled(
            "Gear is OK, nothing to do",
            Style::default().fg(Color::Green),
        )
    };

    Paragraph::new(span)
        .alignment(Alignment::Center)
        .render(in_area, buf);
}
