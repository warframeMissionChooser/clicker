//! Crate that solve the Corpus Hacking minigame for the Warframe Clicker application

#![deny(clippy::all)]
#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]
#![allow(clippy::multiple_crate_versions)]

use minigame_lib::{
    image::ImageResult,
    minigames::{DynamicImage, Enigo, MiniGame, RunningMiniGame, WidgetRender},
    tracing::warn,
    utils::KernelFinder,
};

const OPEN_THRESHOLD: f64 = 25.0;
const PATERN_MATCHING_IMAGE_LOCATION: [u32; 2] = [1576, 795];

mod state;
use state::{Cell, State};

mod parser;
mod solver;

pub struct CorpusHacking {
    finder: KernelFinder,
}

pub struct RunningCorpusHacking {
    click_safe: u8,

    last_state: State,
}

impl CorpusHacking {
    /// Create a new Corpus Hacking solver
    ///
    /// # Errors
    /// Will return an error if the saved image could not be read
    pub fn new() -> ImageResult<Self> {
        let kernel = {
            use minigame_lib::image::png::PngDecoder;

            static KERNEL_FILE: &[u8] = include_bytes!("kernel.png");
            let decoder = PngDecoder::new(KERNEL_FILE)?;
            DynamicImage::from_decoder(decoder)?.to_rgb8()
        };

        Ok(Self {
            finder: KernelFinder::new(
                kernel,
                PATERN_MATCHING_IMAGE_LOCATION,
                OPEN_THRESHOLD,
            ),
        })
    }
}

impl RunningCorpusHacking {
    fn get_state(
        &mut self,
        finder: &KernelFinder,
        img: &DynamicImage,
    ) -> Option<Vec<i8>> {
        if !finder.check(&img.to_rgb8()) {
            return None;
        }

        self.last_state = parser::parse(img);

        // Test if we have at leat 1edge set
        if self
            .last_state
            .iter_flat()
            .flat_map(|(_, cell)| cell.as_array())
            .all(|&b| !b)
        {
            return None;
        }

        let solution = solver::solve(&self.last_state);

        // Test if the solution is non empty
        if solution.iter().all(|&n| n == 0) {
            return None;
        }

        Some(solution)
    }
}

impl MiniGame for CorpusHacking {
    type Running = RunningCorpusHacking;

    const NAME: &'static str = "Corpus Hacking";

    fn should_open(&self, img: &DynamicImage) -> Option<Self::Running> {
        self.finder
            .check(&img.to_rgb8())
            .then(|| RunningCorpusHacking {
                click_safe: 7 * 3,
                last_state: State::default(),
            })
    }
}

impl RunningMiniGame<CorpusHacking> for RunningCorpusHacking {
    fn handle(
        &mut self,
        root: &CorpusHacking,
        img: &DynamicImage,
        enigo: &mut Enigo,
    ) -> bool {
        use minigame_lib::enigo::{MouseButton, MouseControllable};

        let solution = match self.get_state(&root.finder, img) {
            Some(s) => s,
            None => return false,
        };

        let mut mooved = false;
        for (pos, nb) in solution.into_iter().enumerate() {
            let button = match nb {
                0 => continue,
                1..=i8::MAX => MouseButton::Left,
                i8::MIN..=-1 => MouseButton::Right,
            };

            self.click_safe = self.click_safe.saturating_sub(1);
            if self.click_safe == 0 {
                warn!("To many click for that puzzle, waiting next one");
                break;
            }

            mooved = true;

            // Go to pos
            match pos {
                6 => enigo.mouse_move_to(3452 - 1920, 294),
                4 => enigo.mouse_move_to(3680 - 1920, 294),
                5 => enigo.mouse_move_to(3349 - 1920, 481),
                3 => enigo.mouse_move_to(3553 - 1920, 494),
                1 => enigo.mouse_move_to(3799 - 1920, 513),
                2 => enigo.mouse_move_to(3442 - 1920, 679),
                0 => enigo.mouse_move_to(3659 - 1920, 707),
                _ => unreachable!(),
            }

            for _ in 0..i8::abs(nb) {
                std::thread::sleep(std::time::Duration::from_millis(20));
                enigo.mouse_click(button);
            }
        }

        if mooved {
            std::thread::sleep(std::time::Duration::from_millis(20));
            enigo.mouse_move_to(2560 / 2, 1080 / 2);
        }

        true
    }

    fn render(&self) -> WidgetRender {
        let state = self.last_state.clone();
        WidgetRender::new_centered_canvas(
            CorpusHacking::NAME.to_string(),
            move |ctx| {
                use minigame_lib::tui::{style::Color, widgets::canvas::Line};
                use render::Hexagon;

                const SIZE: f64 = 0.3849; // 2 / (3 * sqrt(3)) Can't use const with floats
                for (id, (pos, edge)) in state.iter_full().enumerate() {
                    let q = f64::from(pos.x);
                    let r = f64::from(-pos.x - pos.y);
                    let center = (
                        SIZE * 3.0_f64.sqrt().mul_add(q, 3.0_f64.sqrt() / 2.0 * r),
                        -SIZE * (3.0 / 2.0 * r),
                    );
                    let mut hex = Hexagon {
                        center,
                        size: SIZE,
                        color: Color::Green,
                        pointy_top: true,
                    };

                    ctx.draw(&hex);
                    if let Some(edge) = edge {
                        hex.pointy_top = false;
                        hex.points()
                            .zip(edge.as_array())
                            .filter(|(_, &is_present)| is_present)
                            .for_each(|((x2, y2), _)| {
                                ctx.draw(&Line {
                                    x1: center.0,
                                    y1: center.1,
                                    x2,
                                    y2,
                                    color: Color::Red,
                                });
                            })
                    }

                    let text = format!("{} -> [{} {}]", id, q, r);
                    let text = Box::leak(Box::new(text));
                    ctx.print(center.0, center.1, text, Color::Blue)
                }
            },
        )
    }
}

mod render;
