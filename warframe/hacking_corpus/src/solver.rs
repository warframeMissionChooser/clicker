use super::{Cell, State};
use minigame_lib::tracing::error;

use beehive::Direction;
use pathfinding::directed::astar::astar;

fn rotate(cell: &mut Cell, left: bool) {
    rotate_nb(cell, if left { 1 } else { -1 })
}

fn rotate_nb(cell: &mut Cell, nb: i8) {
    use std::convert::TryInto;

    let elems = &mut cell.as_mut_slice()[..6];
    match nb {
        i if i > 0 => elems.rotate_right(nb.try_into().unwrap()),
        i if i < 0 => elems.rotate_left((-nb).try_into().unwrap()),
        _ => (),
    }
}

fn num_err(state: &State) -> usize {
    state
        .iter_flat()
        .map(|(id_cell, cell)| {
            Direction::iter_planar()
                // Only look direction we have a branch
                .filter(|&dir| *cell.get(dir))
                // Check if target has a brnach facing us
                .filter(|&dir| {
                    let target_point = id_cell + dir;

                    let target = state.get_axial(target_point);

                    if let Some(Some(target)) = target {
                        // Only keep direction if a branch does not exist
                        !target.get(-dir)
                    } else {
                        // Toward empty or inexistent cell
                        true
                    }
                })
                .count()
        })
        .sum()
}

fn is_done(state: &State) -> bool {
    num_err(state) == 0
}

fn possible_actions(state: &State) -> Vec<(State, usize)> {
    state
        .iter_flat()
        .flat_map(|(pos, cell)| {
            [true, false].iter().map(move |is_left| {
                let mut new_state = state.clone();
                let mut cell_left = *cell;
                rotate(&mut cell_left, *is_left);

                *new_state
                    .get_mut_axial(pos)
                    .expect("Id still make sense in cloned value") =
                    Some(cell_left);

                new_state
            })
        })
        .map(|e: State| (e, 1))
        .collect()
}

pub fn solve(state: &State) -> Vec<i8> {
    let result = if let Some(res) = astar(state, possible_actions, num_err, is_done) {
        res
    } else {
        error!("Unable to find a solution, skipping");
        return vec![0; 7];
    };

    let final_state = result.0.last().unwrap();

    num_err(final_state);

    final_state
        .iter_full()
        .zip(state.iter_full())
        .map(|((_, fin), (_, src))| {
            let (src, fin) = match (src, fin) {
                (Some(src), Some(fin)) => (src, fin),
                (None, None) => return 0,
                _ => unreachable!(),
            };
            (-2..=3)
                .filter(|&i| {
                    let mut cell = *src;
                    rotate_nb(&mut cell, i);
                    cell == *fin
                })
                .min_by_key(|&i| i8::abs(i))
                .unwrap_or_else(||panic!(
                    "Solution to contain only rotation from base version: {:?} -> {:?}",
                    src, fin
                ))
        })
        .collect()

    // vec![]
}

#[test]
fn test() {
    use beehive::{DirectionMap, HexMap, PointAxial, QuadPrism};
    use pathfinding::num_traits::Signed;
    let area =
        QuadPrism::new(PointAxial::new(-1, -1, 0), PointAxial::new(2, 2, 1));
    let points: Vec<_> = area.points().filter(|p| p.z().abs() <= 1).collect();

    assert_eq!(points.len(), 7);

    let mut map = HexMap::create(area, None);
    *map.get_mut(points[0]).unwrap() = Some(DirectionMap::from_array([
        false, false, false, false, true, false, false, false,
    ]));
    *map.get_mut(points[1]).unwrap() = Some(DirectionMap::from_array([
        false, false, false, true, false, false, false, false,
    ]));

    assert_eq!(solve(&State::from(map)), vec![3, 1, 0, 0, 0, 0, 0,]);

    let mut map = HexMap::create(area, None);
    *map.get_mut(points[2]).unwrap() = Some(DirectionMap::from_array([
        false, false, false, true, false, false, false, false,
    ]));
    *map.get_mut(points[3]).unwrap() = Some(DirectionMap::from_array([
        false, false, true, false, true, false, false, false,
    ]));
    *map.get_mut(points[6]).unwrap() = Some(DirectionMap::from_array([
        false, false, false, true, false, false, false, false,
    ]));
    assert_eq!(solve(&State::from(map)), vec![0, 0, -2, 2, 0, 0, 0]);
}

fn _debug(state: &State) {
    let out = [
        r"     / \   / \",
        r"    /   \ /   \",
        r"   |     |     |",
        r"   |     |     |",
        r"  / \   / \   / \",
        r" /   \ /   \ /   \",
        r"|     |     |     |",
        r"|     |     |     |",
        r" \   / \   / \   /",
        r"  \ /   \ /   \ /",
        r"   |     |     |",
        r"   |     |     |",
        r"    \   / \   /",
        r"     \ /   \ /",
    ];

    let mut out: Vec<Vec<char>> =
        out.iter().map(|e| e.chars().collect()).collect();

    state.iter().for_each(|(pos, cell)| {
        let (line, col) = match (pos.x(), pos.y()) {
            (0, 1) => (2, 4),
            (1, 0) => (2, 10),
            (-1, 1) => (6, 1),
            (0, 0) => (6, 7),
            (1, -1) => (6, 13),
            (-1, 0) => (10, 4),
            (0, -1) => (10, 10),
            _ => return, //unreachable!()
        };

        match cell {
            Some(cell) => {
                if *cell.get(Direction::YZ) {
                    out[line][col + 1] = '\\';
                }
                if *cell.get(Direction::XZ) {
                    out[line][col + 3] = '/';
                }
                if *cell.get(Direction::XY) {
                    out[line][col + 4] = '_';
                }
                if *cell.get(Direction::ZY) {
                    out[line + 1][col + 3] = '\\';
                }
                if *cell.get(Direction::ZX) {
                    out[line + 1][col + 1] = '/';
                }
                if *cell.get(Direction::YX) {
                    out[line][col] = '_';
                }
            }
            None => {
                let msg = "EMPTY";
                for (i, c) in msg.chars().enumerate() {
                    out[line][col + i] = c;
                }
            }
        }
    });

    /*
    for line in &out {
        for c in line {
            print!("{}", c)
        }
        println!("")
    }*/
}
