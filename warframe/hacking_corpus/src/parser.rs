use beehive::{HexMap, PointAxial, QuadPrism};
use minigame_lib::image::{DynamicImage, GenericImage, GenericImageView, Luma};

const SNAPSHOT_SIZE: u32 = 200;

// Cell order
//   6 4
//  5 3 1
//   2 0
const ITEM_LOCATION: [[u32; 2]; 7] = [
    [1740, 707],
    [1876, 511],
    [1523, 677],
    [1634, 497],
    [1759, 291],
    [1430, 482],
    [1533, 295],
];

const POINT_SIZE: u32 = 32;
const REL_BRANCH: [[u32; 2]; 6] = [
    [57, 20],
    [152, 24],
    [196, 108],
    [144, 184],
    [51, 175],
    [9, 81],
];

use super::{Cell, State};

pub fn parse(img: &DynamicImage) -> State {
    use pathfinding::num_traits::Signed;
    let area =
        QuadPrism::new(PointAxial::new(-1, -1, 0), PointAxial::new(2, 2, 1));

    let mut map = HexMap::create(area, None);

    let img = img.to_luma8();
    map.iter_mut()
        .filter(|(coord, _)| coord.z().abs() <= 1)
        .zip(ITEM_LOCATION.iter())
        .for_each(|elem| {
            let [x, y] = elem.1;

            let part = img.view(
                x - SNAPSHOT_SIZE / 2,
                y - SNAPSHOT_SIZE / 2,
                SNAPSHOT_SIZE,
                SNAPSHOT_SIZE,
            );

            *(elem.0).1 = get_state(&part);
        });

    State::from(map)
}

fn get_state<I: GenericImageView<Pixel = Luma<u8>>>(img: &I) -> Option<Cell>
where
    <I as GenericImageView>::InnerImageView: GenericImage + 'static,
{
    use minigame_lib::utils::average_color;

    let center = img.view(100 - 4, 100 - 4, 8, 8);
    /*
    img.view(0, 0, 200, 200)
        .to_image()
        .save(format!("OUT/corpus_{}.png", i))
        .unwrap();
    center
        .to_image()
        .save(format!("OUT/corpus_{}_c.png", i))
        .unwrap();
        */

    let col: f64 = average_color(&center).iter().sum();
    if col < 200.0 {
        return None;
    }

    let mut res = [false; 8];
    for (_id, (res, pos)) in
        res[..6].iter_mut().zip(REL_BRANCH.iter()).enumerate()
    {
        let dot = img.view(
            pos[0] - POINT_SIZE / 2,
            pos[1] - POINT_SIZE / 2,
            POINT_SIZE,
            POINT_SIZE,
        );

        /*
        dot.to_image()
            .save(format!("OUT/corpus_{}_{}.png", i, id))
            .unwrap();
            */

        let col = minigame_lib::imageproc::template_matching::find_extremes(
            &dot.to_image(),
        )
        .max_value;

        *res = col > 200;
    }

    Some(beehive::DirectionMap::from_array(res))
}
