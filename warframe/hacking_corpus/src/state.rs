use beehive::{DirectionMap, HexMap, PointAxial, QuadPrism};

pub type Cell = DirectionMap<bool>;

type InnerState = HexMap<Option<Cell>>;

#[derive(Eq, Clone)]
pub struct State(InnerState);

impl Default for State {
    fn default() -> Self {
        let area = QuadPrism::new(
            PointAxial::new(-1, -1, 0),
            PointAxial::new(2, 2, 1),
        );

        Self(HexMap::create(area, None))
    }
}

impl From<InnerState> for State {
    fn from(s: InnerState) -> Self {
        Self(s)
    }
}

impl PartialEq for State {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl std::hash::Hash for State {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        for (_, el) in self.0.iter() {
            el.hash(state);
        }
    }
}

impl std::ops::Deref for State {
    type Target = InnerState;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::ops::DerefMut for State {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl State {
    pub fn iter_flat(
        &self,
    ) -> impl Iterator<
        Item = (beehive::PointAxial<i16>, &beehive::DirectionMap<bool>),
    > + '_ {
        self.iter_axial()
            .flat_map(|(id, cell)| cell.as_ref().map(|cell| (id, cell)))
    }

    pub fn iter_full(
        &self,
    ) -> impl Iterator<
        Item = (
            beehive::PointAxial<i16>,
            &Option<beehive::DirectionMap<bool>>,
        ),
    > + '_ {
        self.iter_axial()
            .filter(|(coord, _)| (coord.x + coord.y).abs() <= 1)
    }
}
