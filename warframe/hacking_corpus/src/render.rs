use minigame_lib::tui::{
    style::Color,
    widgets::canvas::{Line, Painter, Shape},
};

pub struct Hexagon {
    pub center: (f64, f64),
    pub size: f64,
    pub color: Color,
    pub pointy_top: bool,
}

impl Hexagon {
    /// Return an iterator over all points for the circle
    #[inline]
    pub fn points(&self) -> impl Iterator<Item = (f64, f64)> + '_ {
        use std::f64::consts::{FRAC_PI_2, FRAC_PI_3};
        let delta = if self.pointy_top {
            FRAC_PI_2
        } else {
            2.0 * FRAC_PI_3
        };
        (0..6).map(move |i| {
            let i: f64 = i.into();
            let (sin, cos) = (delta - FRAC_PI_3 * i).sin_cos();

            (
                self.size.mul_add(cos, self.center.0),
                self.size.mul_add(sin, self.center.1),
            )
        })
    }
}

impl Shape for Hexagon {
    fn draw(&self, painter: &mut Painter) {
        let mut points = self.points().collect::<Vec<_>>();

        points.push(*points.first().unwrap());

        points.windows(2).for_each(|a| match *a {
            [(x1, y1), (x2, y2)] => Line {
                x1,
                y1,
                x2,
                y2,
                color: self.color,
            }
            .draw(painter),

            _ => unreachable!(),
        })
    }
}
