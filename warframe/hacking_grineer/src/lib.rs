//! Crate that solve the Grinner Hacking minigame for the Warframe Clicker application

#![deny(clippy::all)]
#![warn(clippy::cargo, clippy::nursery, clippy::pedantic)]
#![allow(clippy::multiple_crate_versions)]

use minigame_lib::{
    image::ImageResult,
    minigames::{DynamicImage, Enigo, MiniGame, RunningMiniGame, WidgetRender},
    tracing::warn,
    utils::KernelFinder,
};
use state::ScreenParser;

/// Module that handle parsing of screen and the creation of state
mod state;

/// Threshold used in kernel detection
const OPEN_THRESHOLD: f64 = 25.0;

/// Center of the kernel search
const IMAGE_LOCATION: [u32; 2] = [1571, 401];

/// Grinner Hacking minigame
pub struct GrineerHacking {
    /// Finder used to detect if the minigame is open
    finder: KernelFinder,
}

pub struct RunningGrineerHacking {
    /// Parse the screen and return the state
    screen_parser: state::ScreenParser,

    /// Last saved state, used in debug render
    last_state: Vec<state::State>,
}

impl GrineerHacking {
    /// Create a new Greener Hacking solver
    ///
    /// # Errors
    /// Will return an error if the saved image could not be read
    #[inline]
    pub fn new() -> ImageResult<Self> {
        let kernel = {
            use minigame_lib::image::png::PngDecoder;

            /// Image containing the Kernel to look for
            static KERNEL_FILE: &[u8] = include_bytes!("kernel.png");
            let decoder = PngDecoder::new(KERNEL_FILE)?;
            DynamicImage::from_decoder(decoder)?.to_rgb8()
        };

        Ok(Self {
            finder: KernelFinder::new(kernel, IMAGE_LOCATION, OPEN_THRESHOLD),
        })
    }
}

impl MiniGame for GrineerHacking {
    type Running = RunningGrineerHacking;

    const NAME: &'static str = "Grineer Hacking";

    fn should_open(&self, img: &DynamicImage) -> Option<Self::Running> {
        self.finder
            .check(&img.to_rgb8())
            .then(|| RunningGrineerHacking {
                screen_parser: ScreenParser::new(),
                last_state: Vec::new(),
            })
    }
}

impl RunningMiniGame<GrineerHacking> for RunningGrineerHacking {
    fn handle(
        &mut self,
        root: &GrineerHacking,
        img: &DynamicImage,
        enigo: &mut Enigo,
    ) -> bool {
        use minigame_lib::enigo::{Key::Space, KeyboardControllable};

        if !root.finder.check(&img.to_rgb8()) {
            return false;
        }

        self.last_state = self.screen_parser.parse(&img.to_luma8());

        if self.screen_parser.get_action(&self.last_state) {
            enigo.key_click(Space);
        }

        true
    }

    #[inline]
    fn render(&self) -> WidgetRender {
        use minigame_lib::tui::{
            style::Color, utils::circle::Circle, widgets::canvas::Line,
        };

        let last_state = self.last_state.clone();
        WidgetRender::new_centered_canvas(
            GrineerHacking::NAME.to_string(),
            move |ctx| {
                ctx.draw(&Circle {
                    color: Color::Red,
                    ..Circle::default()
                });

                let c = Circle {
                    resolution: 8,
                    ..Circle::default()
                };

                for (from, state) in c.angles().zip(last_state.iter()) {
                    let from = 90.0 - from;

                    let hover = match state {
                        state::State::Emtpy => continue,
                        state::State::Present => 0.7,
                        state::State::Hovered => 1.0,
                    };

                    ctx.draw(&Line {
                        x1: hover * from.cos(),
                        y1: hover * from.sin(),
                        x2: 0.2 * from.cos(),
                        y2: 0.2 * from.sin(),
                        color: Color::LightRed,
                    });
                }
            },
        )
    }
}
