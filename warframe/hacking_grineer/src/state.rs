use minigame_lib::{
    image::{GenericImageView, GrayImage},
    tracing::debug,
};

/// Possible state for a chunk
/// There is no difference between a validated chunk and no chunk
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum State {
    /// Either the chunk is in a valid position, or there is no chunk
    Emtpy,

    /// The chunk is present, but not hovered
    Present,

    /// The chunk is present and is hovered
    Hovered,
}

use super::IMAGE_LOCATION as CENTER;

/// Positions of all chuncks
const POSITIONS: [[i32; 2]; 8] = [
    [79, -261],
    [306, -170],
    [398, 74],
    [276, 305],
    [48, 360],
    [-138, 247],
    [-207, 44],
    [-130, -158],
];

/// Sensibility used to detect if a chunk is present
const DEFAULT_SENSIBILITY_HI: u8 = 200;

/// Default sensibility used to detect if the chunk is hovered or not
const DEFAULT_SENSIBILITY_LO: u8 = 50;

/// Sensibility increment
const DEFAULT_SENSIBILITY_INC: u8 = 10;

/// Parse the screen and return the state. Store internal sensibility to adapt to backround screen
pub struct ScreenParser {
    /// Sensibility for each chunk
    sensibility: [u8; 8],

    /// Last action done. Used to adjust sensibility
    last_action: Option<usize>,
}

impl ScreenParser {
    /// Create a new parser
    pub const fn new() -> Self {
        Self {
            last_action: None,
            sensibility: [DEFAULT_SENSIBILITY_LO; 8],
        }
    }

    /// Parse the provided image in a state
    pub fn parse(&mut self, frame: &GrayImage) -> Vec<State> {
        use std::convert::TryInto;

        let center_x: i32 = CENTER[0].try_into().unwrap_or(0);
        let center_y: i32 = CENTER[1].try_into().unwrap_or(0);
        POSITIONS
            .iter()
            .enumerate()
            .map(|(i, &[x, y])| {
                let part = frame
                    .view(
                        center_x.saturating_add(x).try_into().unwrap_or(0),
                        center_y.saturating_add(y).try_into().unwrap_or(0),
                        8,
                        8,
                    )
                    .to_image();

                let res =
                    minigame_lib::imageproc::template_matching::find_extremes(
                        &part,
                    );

                //dbg!(res);

                if res.max_value > DEFAULT_SENSIBILITY_HI {
                    if res.min_value
                        > self.sensibility.get(i).cloned().unwrap_or(255)
                    {
                        State::Hovered
                    } else {
                        State::Present
                    }
                } else {
                    State::Emtpy
                }
            })
            .collect()
    }

    /// Return true if we should press the Space key for the provided state
    /// This function will also adjust internal sensibility when required
    pub fn get_action(&mut self, states: &[State]) -> bool {
        let option = states.iter().position(|&e| e == State::Hovered);

        let should_press = option.is_some();

        // Reduce sensibility if we try the same action as last time
        // This mean we pressed space, and the chunck didn't move, so it was a false detection
        if let Some(opt) = option {
            if option == self.last_action {
                #[allow(clippy::expect_used)]
                // If this happend, this is an internal bug
                let sens =
                    self.sensibility.get_mut(opt).expect("Index should be < 8");

                *sens = sens.saturating_add(DEFAULT_SENSIBILITY_INC);
                debug!("Increasing sensibility for {} to {}", opt, sens);
            }
        }

        self.last_action = option;

        should_press
    }
}
